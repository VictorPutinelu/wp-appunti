# Appunti creazione di un tema
⚠️ Il tema potrebbe non funzionare correttamente se è stato copiato direttamente in ```wp-content/themes/``` e non è importato utilizzando l'interfaccia di WP

### TODO
- [ ] Leggere la documentazione sulla creazione di un tema
- [ ] Cambiare il nome del cartella, trovare e sostiturie le stringhe - ```customtheme``` a ```vic-theme```
- [ ] Impostare correttamente la Timezone predefinita
- [x] Rimuovere il CDN di Bootstrap da ```header.php```
- [x] Includere Bootstrap nel progetto


### Configurazione del debugger ```wp-config.php```
```php
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);
```

### Plugins
- Debug Bar
- Query Monitor
- Log Deprecated Notices
- FakerPress

## Handbook links
- [Common WordPress template files](https://developer.wordpress.org/themes/basics/template-files/#common-wordpress-template-files)
- [Using template files](https://developer.wordpress.org/themes/basics/template-files/#using-template-files)
- [Post Types](https://developer.wordpress.org/themes/basics/post-types/)
- [Theme folder and file structure](https://developer.wordpress.org/themes/basics/organizing-theme-files/#theme-folder-and-file-structure)
- [Template hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/)
- [What the loop can display](https://developer.wordpress.org/themes/basics/the-loop/#what-the-loop-can-display)

