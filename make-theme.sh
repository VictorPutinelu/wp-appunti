#!/bin/bash
FOLDER="customtheme"
TMP_FOLDER="tmp_"$FOLDER

cp -R ./$FOLDER ./$TMP_FOLDER &&
	 sudo chown -R www-data:www-data ./$TMP_FOLDER &&
	 zip -r customtheme.zip ./$TMP_FOLDER &&
	 sudo rm -rf ./$TMP_FOLDER 

echo "completed at: `date`"
